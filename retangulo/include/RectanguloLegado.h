#ifndef RECTANGULOLEGADO_H
#define RECTANGULOLEGADO_H

#include <iostream>

using namespace std;

class RectanguloLegado
{
    public:
        RectanguloLegado(int x1_,int x2_,int y1_,int y2_);
        void oldDraw();
        virtual ~RectanguloLegado();

    protected:

    private:
        int x1;
        int x2;
        int y1;
        int y2;
};

#endif // RECTANGULOLEGADO_H
