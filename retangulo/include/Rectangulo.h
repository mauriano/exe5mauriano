#ifndef RECTANGULO_H
#define RECTANGULO_H

#include <iostream>

using namespace std;

class Rectangulo
{
    public:
        Rectangulo();
        virtual void draw() = 0;
        virtual ~Rectangulo();

    protected:

    private:
};

#endif // RECTANGULO_H
