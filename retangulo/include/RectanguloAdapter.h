#ifndef RECTANGULOADAPTER_H
#define RECTANGULOADAPTER_H


#include<Rectangulo.h>
#include<RectanguloLegado.h>

#include <iostream>

using namespace std;

class RectanguloAdapter: public Rectangulo, private RectanguloLegado
{
    public:
        RectanguloAdapter(int x, int y, int w, int h);
        void draw();

        virtual ~RectanguloAdapter();

    protected:

    private:
};

#endif // RECTANGULOADAPTER_H
